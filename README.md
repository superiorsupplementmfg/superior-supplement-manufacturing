Custom Supplement & Private Label Manufacturer. R&D, Formulation, Capsules, Tablets, Softgel, Gummy, Liquids, Powders, Packaging, CBD and more

Turnkey manufacturing from concept to finished product. Small and large batch R&D, Pilots and Manufacturing.

Website: https://www.superiorsupplementmfg.com
